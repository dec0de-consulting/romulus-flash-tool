/*
    This file the ROMulus GUI.

    This file was created by Andrew (LinuxJedi) Hutchings.

    ROMulus GUI is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    ROMulus GUI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ROMulus GUI. If not, see <http://www.gnu.org/licenses/>.
*/

#include <proto/intuition.h>
#include <proto/dos.h>
#include <intuition/intuition.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <libraries/reqtools.h>
#include <clib/reqtools_protos.h>
#include <workbench/startup.h>
#include <clib/expansion_protos.h>

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "RomInfo.h"
#include "Helpers.h"
#include "FlashDeviceSST39.h"

// Not in any headers for some reason!
struct ConfigDev * __stdargs FindConfigDev( CONST struct ConfigDev *oldConfigDev, LONG manufacturer, LONG product );

struct ReqToolsBase *ReqToolsBase;
struct Library *ExpansionBase = NULL;

#define VERSION "v1.5"

#define MAX_BOARD_VERSION 0x000010ffUL

#define CMD_BANK2 0x3000
#define CMD_BANK1 0x1000
#define CMD_DISABLE 0x0000

#define BASE_ADDR 0x00A00000UL
#define MB_ROM_ADDR 0x00F80000UL

#define MANUFACTURER 0x1448
#define MODEL 1

#define LOOP_TIMEOUT        (ULONG)10000
#define KICKSTART_256K      (ULONG)(256 * 1024)

#define FILE_BUFFER 16384

#define button1w 66
#define button1h 20

#define button2w 107
#define button2h 20

#define button3w 128
#define button3h 20

#define tbox1w 324
#define tbox1h 18

struct TextAttr font =
{
    (STRPTR)"topaz.font",
    9,
    FS_NORMAL,
    FPF_ROMFONT|FPF_DESIGNED
};

SHORT SharedBordersPairs0[] =
{
    0, 0, 0, button1h - 1, 1, button1h - 2, 1, 0, button1w - 2, 0
};
SHORT SharedBordersPairs1[] =
{
    1, button1h - 1, button1w - 2, button1h - 1, button1w - 2, 1, button1w - 1, 0, button1w - 1, button1h - 1
};

SHORT SharedBordersPairs4[] =
{
    0, 0, 0, button2h - 1, 1, button2h - 2, 1, 0, button2w - 2, 0
};
SHORT SharedBordersPairs5[] =
{
    1, button2h - 1, button2w - 2, button2h - 1, button2w - 2, 1, button2w - 1, 0, button2w - 1, button2h - 1
};

SHORT SharedBordersPairs2[] =
{
    0, 0, 0, button3h - 1, 1, button3h - 2, 1, 0, button3w - 2, 0
};
SHORT SharedBordersPairs3[] =
{
    1, button3h - 1, button3w - 2, button3h - 1, button3w - 2, 1, button3w - 1, 0, button3w - 1, button3h - 1
};


SHORT SharedBordersPairs6[] =
{
    -2, -1, -2, tbox1h - 1, -1, tbox1h - 2, -1, -1, tbox1w - 2, -1
};
SHORT SharedBordersPairs7[] =
{
    -1, tbox1h - 1, tbox1w - 2, tbox1h - 1, tbox1w - 2, 0, tbox1w - 1, -1, tbox1w - 1, tbox1h - 1
};


struct Border SharedBorders[] =
{
    {0, 0, 2, 0, JAM2, 5, (SHORT *) &SharedBordersPairs0[0], &SharedBorders[1]},
    {0, 0, 1, 0, JAM2, 5, (SHORT *) &SharedBordersPairs1[0], NULL},
    {0, 0, 2, 0, JAM2, 5, (SHORT *) &SharedBordersPairs2[0], &SharedBorders[3]},
    {0, 0, 1, 0, JAM2, 5, (SHORT *) &SharedBordersPairs3[0], NULL},
    {0, 0, 2, 0, JAM2, 5, (SHORT *) &SharedBordersPairs4[0], &SharedBorders[5]},
    {0, 0, 1, 0, JAM2, 5, (SHORT *) &SharedBordersPairs5[0], NULL},
    {0, 0, 1, 0, JAM2, 5, (SHORT *) &SharedBordersPairs6[0], &SharedBorders[7]},
    {0, 0, 2, 0, JAM2, 5, (SHORT *) &SharedBordersPairs7[0], NULL},
};

struct Border SharedBordersInvert[] =
{
    {0, 0, 1, 0, JAM2, 5, (SHORT *) &SharedBordersPairs0[0], &SharedBordersInvert[1]},
    {0, 0, 2, 0, JAM2, 5, (SHORT *) &SharedBordersPairs1[0], NULL},
    {0, 0, 1, 0, JAM2, 5, (SHORT *) &SharedBordersPairs2[0], &SharedBordersInvert[3]},
    {0, 0, 2, 0, JAM2, 5, (SHORT *) &SharedBordersPairs3[0], NULL},
    {0, 0, 1, 0, JAM2, 5, (SHORT *) &SharedBordersPairs4[0], &SharedBordersInvert[5]},
    {0, 0, 2, 0, JAM2, 5, (SHORT *) &SharedBordersPairs5[0], NULL},
};

struct IntuiText QuitButton_text =
{
    1, 0, JAM2, 12, 6, &font, (UBYTE *)"Quit", NULL
};

#define GADQUIT 8

struct Gadget QuitButton =
{
    NULL, 518, 102, button1w, button1h,
    GADGHIMAGE,
    RELVERIFY,
    BOOLGADGET,
    (APTR) &SharedBorders[0], (APTR) &SharedBordersInvert[0],
    &QuitButton_text, 0, NULL, GADQUIT, NULL
};

struct IntuiText AboutButton_text =
{
    1, 0, JAM2, 8, 6, &font, (UBYTE *)"About", NULL
};

#define GADABOUT 7

struct Gadget AboutButton =
{
    &QuitButton, 436, 102, button1w, button1h,
    GADGHIMAGE,
    RELVERIFY,
    BOOLGADGET,
    (APTR) &SharedBorders[0], (APTR) &SharedBordersInvert[0],
    &AboutButton_text, 0, NULL, GADABOUT, NULL
};

struct IntuiText EraseButton_text =
{
    1, 0, JAM2, 8, 6, &font, (UBYTE *)"Erase ROMs", NULL
};

#define GADERASE 6

struct Gadget EraseButton =
{
    &AboutButton, 20, 102, button3w, button3h,
    GADGHIMAGE,
    RELVERIFY,
    BOOLGADGET,
    (APTR) &SharedBorders[2], (APTR) &SharedBordersInvert[2],
    &EraseButton_text, 0, NULL, GADERASE, NULL
};

struct IntuiText LoadFile_text =
{
    1, 0, JAM2, 8, 6, &font, (UBYTE *)"Load File", NULL
};

#define GADFILE2 5

struct Gadget LoadFile2 =
{
    &EraseButton, 482, 69, button2w, button2h,
    GADGHIMAGE,
    RELVERIFY,
    BOOLGADGET,
    (APTR) &SharedBorders[4], (APTR) &SharedBordersInvert[4],
    &LoadFile_text, 0, NULL, GADFILE2, NULL
};

#define GADFILE1 4

struct Gadget LoadFile1 =
{
    &LoadFile2, 482, 45, button2w, button2h,
    GADGHIMAGE,
    RELVERIFY,
    BOOLGADGET,
    (APTR) &SharedBorders[4], (APTR) &SharedBordersInvert[4],
    &LoadFile_text, 0, NULL, GADFILE1, NULL
};

#define GADFILEROM 3

struct IntuiText SaveROM_text =
{
    1, 0, JAM2, 10, 6, &font, (UBYTE *)"Save ROM", NULL
};

struct Gadget SaveROM =
{
    &LoadFile1, 482, 21, button2w, button2h,
    GADGHIMAGE,
    RELVERIFY,
    BOOLGADGET,
    (APTR) &SharedBorders[4], (APTR) &SharedBordersInvert[4],
    &SaveROM_text, 0, NULL, GADFILEROM, NULL
};

UBYTE FlashROM2_buf[64];

struct IntuiText FlashROM2_text[] =
{
    {1, 0, JAM2, -124, 4, &font, (UBYTE *)"FLASH ROM 2:", &FlashROM2_text[1]},
    {1, 0, JAM2, 4, 4, &font, (UBYTE *)FlashROM2_buf, NULL}
};

#define GADFLASH2 2

struct Gadget FlashROM2 =
{
    &SaveROM, 154, 70, tbox1w, tbox1h,
    GADGHIMAGE,
    0,
    BOOLGADGET,
    (APTR) &SharedBorders[6], NULL,
    &FlashROM2_text[0], 0, NULL, GADFLASH2, NULL
};

UBYTE FlashROM1_buf[64];

struct IntuiText FlashROM1_text[] =
{
    {1, 0, JAM2, -124, 4, &font, (UBYTE *)"FLASH ROM 1:", &FlashROM1_text[1]},
    {1, 0, JAM2, 4, 4, &font, (UBYTE *)FlashROM1_buf, NULL}
};

#define GADFLASH1 1

struct Gadget FlashROM1 =
{
    &FlashROM2, 154, 46, tbox1w, tbox1h,
    GADGHIMAGE,
    0,
    BOOLGADGET,
    (APTR) &SharedBorders[6], NULL,
    &FlashROM1_text[0], 0, NULL, GADFLASH1, NULL
};

UBYTE Motherboard_buf[64];

struct IntuiText Motherboard_text[] =
{
    {1, 0, JAM2, -134, 4, &font, (UBYTE *)"Internal ROM:", &Motherboard_text[1]},
    {1, 0, JAM2, 4, 4, &font, (UBYTE *)Motherboard_buf, NULL}
};

#define GADMOTHER 0

struct Gadget Motherboard =
{
    &FlashROM1, 154, 22, tbox1w, tbox1h,
    GADGHIMAGE,
    0,
    BOOLGADGET,
    (APTR) &SharedBorders[6], NULL,
    &Motherboard_text[0], 0, NULL, GADMOTHER, NULL
};


static void WriteRomText(const char *text, UBYTE *buffer, struct Window *window,
                         struct Gadget *gadget)
{
    ULONG newlen = strlen(text);
    ULONG oldlen = strlen((char *)buffer);

    if (newlen < oldlen)
    {
        snprintf((char *)buffer, 64, "%s%*.*s", text, (int)(oldlen - newlen),
                 (int)(oldlen - newlen), " ");
    }
    else
    {
        strncpy((char *)buffer, text, 64);
    }

    RefreshGadgets(gadget, window, NULL);
}

static char *GetFile(BYTE save)
{
    struct rtFileRequester *filereq;
    char filename[128];
    char *fullpath = malloc(256 * sizeof(char));
    FILE *romFile;
    struct romInfo romInfo;
    UBYTE *buf = NULL;

    if ((filereq = (struct rtFileRequester*)rtAllocRequestA (RT_FILEREQ, NULL)))
    {
        filename[0] = 0;

        if (!save)
        {
            if (!rtFileRequest(filereq, filename, "Pick a ROM file",
                               TAG_END))
            {
                free(fullpath);
                return NULL;
            }
        }
        else
        {
            strcpy(filename, "kick.rom");
            if (!rtFileRequest(filereq, filename, "Save ROM file", RTFI_Flags,
                               FREQF_SAVE, TAG_END))
            {
                free(fullpath);
                return NULL;
            }
        }

    }
    else
    {
        rtEZRequest("Out of memory!", "Oh no!", NULL, NULL);
    }

    // Turns out WB doesn't like DF0:/filename.rom
    if (filereq->Dir[(strlen(filereq->Dir) - 1)] == ':')
    {
        snprintf(fullpath, 255, "%s%s", filereq->Dir, filename);
    }
    else if (!strlen(filereq->Dir))
    {
        snprintf(fullpath, 255, "%s", filename);
    }
    else
    {
        snprintf(fullpath, 255, "%s/%s", filereq->Dir, filename);
    }

    if (save)
    {
        return fullpath;
    }

    romFile = fopen(fullpath, "r");

    if (!romFile)
    {
        free(fullpath);
        rtEZRequest("Could not open ROM file", "OK", NULL, NULL);
        return NULL;
    }

    buf = (UBYTE *)malloc(1024 * sizeof(UBYTE));
    fread(buf, 1, 1023, romFile);

    if (getRomInfo(buf, &romInfo))
    {
        int res = rtEZRequest("ROM file does not appear to be valid.\n"
                              "It could be byte swapped which will not work.\n"
                              "Continue anyway?",
                              "Yes|No", NULL, NULL);

        if (!res)
        {
            free(fullpath);
            return NULL;
        }
    }

    fclose(romFile);
    return fullpath;
}

static void getRom(int romID, struct ConfigDev *myCD, struct Window *myWindow)
{
    struct romInfo rInfo;
    char *rtext = NULL;

    if (romID == 1)
    {
        *(ULONG*)(myCD->cd_BoardAddr) = CMD_BANK1;
        getRomInfo((UBYTE *)BASE_ADDR, &rInfo);
    }
    else
    {
        *(ULONG*)(myCD->cd_BoardAddr) = CMD_BANK2;
        getRomInfo((UBYTE *)BASE_ADDR, &rInfo);
    }

    displayRomInfo(&rInfo, &rtext);

    if (romID == 1)
    {
        WriteRomText(rtext, FlashROM1_buf, myWindow, &FlashROM1);
    }
    else
    {
        WriteRomText(rtext, FlashROM2_buf, myWindow, &FlashROM2);
    }

    free(rtext);
}

static void eraseFlash(struct ConfigDev *myCD)
{
    if (flashOK == eraseCompleteFlash(BASE_ADDR))
    {
        tFlashCommandStatus flashCommandStatus;
        ULONG breakCount = 0;

        do
        {
            flashCommandStatus = checkFlashStatus(BASE_ADDR);
            breakCount++;
        }
        while ((flashCommandStatus != flashOK) && (breakCount < LOOP_TIMEOUT));

        if (flashOK != flashCommandStatus)
        {
            rtEZRequest("An error occurred erasing the flash chips.", "OK", NULL, NULL);
        }
    }
    else
    {
        rtEZRequest("Flash chips did not accept the erase command.", "OK", NULL, NULL);
    }
}

static void eraseFlashLoop(int romID, struct ConfigDev *myCD)
{
    UBYTE sector = 0;
    UBYTE end = 0;
    ULONG breakCount = 0;
    tFlashCommandStatus flashCommandStatus = flashIdle;

    if (romID == 2)
    {
        end = MAX_SECTORS / 2;
        *(ULONG*)myCD->cd_BoardAddr = CMD_BANK2;
    }
    else
    {
        *(ULONG*)myCD->cd_BoardAddr = CMD_BANK1;
        end = MAX_SECTORS / 2;
    }

    for (; sector < end; sector++)
    {
        flashCommandStatus = eraseFlashSector(BASE_ADDR, sector);
        if (flashCommandStatus != flashOK)
        {
            rtEZRequest("An error occurred erasing the flash chips.", "OK", NULL, NULL);
            return;
        }

        breakCount = 0;
        do
        {
            flashCommandStatus = checkFlashStatus(BASE_ADDR + (sector << 13));
            breakCount++;
        }
        while ((flashCommandStatus != flashOK) && (breakCount < LOOP_TIMEOUT));

        if (breakCount == LOOP_TIMEOUT)
        {
            rtEZRequest("A timeout occurred erasing the flash chips.", "OK", NULL, NULL);
            return;
        }
    }
}

static tFlashCommandStatus programFlashLoop(const UWORD *baseAddress, char *romFile, int romID, int small_state, struct Window *myWindow)
{
    tFlashCommandStatus flashCommandStatus = flashIdle;
    ULONG currentWordIndex = 0;
    ULONG breakCount = 0;
    FILE *rFile = fopen(romFile, "rb");
    volatile UWORD rWord;
    UWORD cWord;
    size_t rCount;
    UWORD *buf = malloc(FILE_BUFFER * sizeof(UWORD));

    if (buf == NULL)
    {
        rtEZRequest("Could not allocate file buffer", "OK", NULL, NULL);
        return flashProgramError;
    }

    if (!rFile)
    {
        rtEZRequest("File open error", "OK", NULL, NULL);
        free(buf);
        return flashProgramError;
    }

    while((rCount = fread(buf, sizeof(UWORD), FILE_BUFFER, rFile)))
    {
        if (rCount != FILE_BUFFER)
        {
            fclose(rFile);
            rtEZRequest("File read error", "OK", NULL, NULL);
            free(buf);
            return flashProgramError;
        }
        for (int i = 0; i < FILE_BUFFER; i++)
        {
            int pcnt;
            char pcnt_buf[30];
            rWord = buf[i];
            writeFlashWord((ULONG)baseAddress, currentWordIndex << 1, rWord);

            breakCount = 0;
            do
            {
                flashCommandStatus = checkFlashStatus((ULONG)((baseAddress) + (currentWordIndex << 1)));
                breakCount++;
            }
            while ((flashCommandStatus != flashOK) && (breakCount < LOOP_TIMEOUT));

            if (flashCommandStatus != flashOK)
            {
                rtEZRequest("Program timeout", "OK", NULL, NULL);
                fclose(rFile);
                free(buf);
                return flashProgramTimeout;
            }

            breakCount = 0;
            // TF boards can read before the write is fully confirmed, retry if this happens
            do
            {
                cWord = baseAddress[currentWordIndex];
                breakCount++;
            }
            while ((rWord != cWord) && (breakCount < LOOP_TIMEOUT));
            if (rWord != cWord)
            {
                char buf[255];
                snprintf(buf, 255, "Data doesn't match, 0x%04x != 0x%04x\nWord number %d\nBaseaddr 0x%06x\nstatus %d\nCount: %d",  rWord, cWord, currentWordIndex, (ULONG)(baseAddress), flashCommandStatus, breakCount);
                rtEZRequest("%s", "OK", NULL, NULL, buf);
                fclose(rFile);
                return flashProgramError;
            }

            currentWordIndex += 1;
            pcnt = (small_state ? (currentWordIndex + 0x400000) : currentWordIndex) / 0x1000000 * 100;
            snprintf(pcnt_buf, 30, "Flashing... %d%% complete", pcnt);
            if (romID == 2)
            {
                WriteRomText(pcnt_buf, FlashROM2_buf, myWindow, &FlashROM2);
            }
            else
            {
                WriteRomText(pcnt_buf, FlashROM1_buf, myWindow, &FlashROM1);
            }

        }
    }
    fclose(rFile);
    free(buf);
    return (flashCommandStatus);
}

static void saveRom(struct Window *myWindow)
{
    struct romInfo rInfo;
    char *romFile = GetFile(1);

    if (romFile)
    {
        FILE *rFile = fopen(romFile, "wb");
        int iterations;
        ULONG offset = 0;
        if (!rFile)
        {
            rtEZRequest("File open error", "OK", NULL, NULL);
            return;
        }
        WriteRomText("Saving...", Motherboard_buf, myWindow, &Motherboard);
        getRomInfo((UBYTE *)MB_ROM_ADDR, &rInfo);
        if (rInfo.id == ROM_TYPE_256)
        {
            iterations = (256*1024) / FILE_BUFFER / sizeof(UWORD);
        }
        else
        {
            iterations = (512*1024) / FILE_BUFFER / sizeof(UWORD);
        }
        for (int i = 0; i < iterations; i++)
        {
            if (fwrite((UWORD*)(MB_ROM_ADDR+offset), sizeof(UWORD), FILE_BUFFER, rFile) != FILE_BUFFER)
            {
                fclose(rFile);
                rtEZRequest("File write error", "OK", NULL, NULL);
                return;
            }
            offset += (FILE_BUFFER * sizeof(UWORD));
        }
        fclose(rFile);
    }
}

static void flashRom(int romID, struct ConfigDev *myCD, struct Window *myWindow)
{
    ULONG fileSize;
    char *romFile = GetFile(0);

    if (romFile)
    {
        tReadFileHandler readFileProgram = getFileSize(romFile, &fileSize);

        if (readFileOK == readFileProgram)
        {
            tFlashCommandStatus programFlashStatus = flashIdle;

            if (romID == 2)
            {
                WriteRomText("Erasing...", FlashROM2_buf, myWindow, &FlashROM2);
            }
            else
            {
                WriteRomText("Erasing...", FlashROM1_buf, myWindow, &FlashROM1);
            }
            eraseFlashLoop(romID, myCD);

            if (romID == 2)
            {
                *(ULONG*)myCD->cd_BoardAddr = CMD_BANK2;
                WriteRomText("Flashing...", FlashROM2_buf, myWindow, &FlashROM2);
            }
            else
            {
                *(ULONG*)myCD->cd_BoardAddr = CMD_BANK1;
                WriteRomText("Flashing...", FlashROM1_buf, myWindow, &FlashROM1);
            }

            programFlashStatus = programFlashLoop((UWORD*)BASE_ADDR, romFile, romID, 0, myWindow);

            if ((programFlashStatus == flashOK) && (fileSize == KICKSTART_256K))
            {
                programFlashStatus = programFlashLoop((UWORD*)BASE_ADDR + KICKSTART_256K, romFile, romID, 1, myWindow);
            }

        }
        else
        {
            rtEZRequest("Could not get the file size for the ROM.", "OK", NULL, NULL);
        }

        free(romFile);
        getRom(romID, myCD, myWindow);
    }

}

void myputs (char *str)
{
    Write (Output(), str, strlen(str));
}

struct NewWindow winlayout =
{
    20, 20,
    600, 128,
    -1, -1,
    CLOSEWINDOW | GADGETUP | GADGETDOWN,
    ACTIVATE | WINDOWCLOSE | WINDOWDRAG | WINDOWDEPTH,
    &Motherboard, NULL,
    (STRPTR)"ROMulus Flash Tool",
    NULL, NULL,
    0, 0,
    600, 400,
    WBENCHSCREEN
};

int main()
{
    struct Window *myWindow;

    IntuitionBase = (struct IntuitionBase *) OpenLibrary ("intuition.library", 0);
    if (IntuitionBase == NULL)
    {
        return 0;
    }

    if (!(ReqToolsBase = (struct ReqToolsBase *)
                         OpenLibrary (REQTOOLSNAME, REQTOOLSVERSION)))
    {
        if (!(ReqToolsBase = (struct ReqToolsBase *)
                         OpenLibrary ("romulus:libs13/reqtools.library", REQTOOLSVERSION)))
        {
            myputs ("You need reqtools.library V38 or higher!\n"
                    "Please install it in your Libs: drirectory.\n");

            exit (RETURN_FAIL);
        }
    }

    /* Open any version expansion.library to read in ConfigDevs */
    ExpansionBase = OpenLibrary("expansion.library", 0L);

    /* Check if opened correctly, otherwise exit with message and error */
    if (NULL == ExpansionBase)
    {
        myputs("Failed to open expansion.library\n");
        exit(RETURN_FAIL);
    }


    myWindow = OpenWindow(&winlayout);
    struct ConfigDev *myCD = FindConfigDev(NULL, MANUFACTURER, MODEL);

    if (NULL == myCD)
    {
        rtEZRequest("ROMulus board not found.\n"
                    "You might be booting from one of its ROMs.\n"
                    "Which means accessing ROMs here is disabled.\n\n"
                    "Please reboot into the Motherboard ROM and try again.",
                    "OK", NULL, NULL);
        OffGadget(&EraseButton, myWindow, NULL);
        OffGadget(&LoadFile1, myWindow, NULL);
        OffGadget(&LoadFile2, myWindow, NULL);
        OffGadget(&FlashROM1, myWindow, NULL);
        OffGadget(&FlashROM2, myWindow, NULL);
    }
    else
    {
        if (myCD->cd_Rom.er_SerialNumber > MAX_BOARD_VERSION)
        {
            rtEZRequest("The firmware in this ROMulus board is too new.\n"
                    "Please download an updated version.",
                    "OK", NULL, NULL);
            exit(RETURN_FAIL);
        }
        getRom(1, myCD, myWindow);
        getRom(2, myCD, myWindow);
    }

    struct romInfo mbInfo;

    char *mbtext = NULL;

    if (!getRomInfo((UBYTE *)MB_ROM_ADDR, &mbInfo))
    {
        displayRomInfo(&mbInfo, &mbtext);
        WriteRomText(mbtext, Motherboard_buf, myWindow, &Motherboard);
        free(mbtext);
    }

    FOREVER
    {
        bool closewin = false;
        struct IntuiMessage *message;
        Wait(1 << myWindow->UserPort->mp_SigBit);

        while ((message = (struct IntuiMessage*)GetMsg(myWindow->UserPort)))
        {
            ULONG class = message->Class;
            struct Gadget *address = (struct Gadget*)message->IAddress;
            ReplyMsg((struct Message*)message);

            if (class == CLOSEWINDOW)
            {
                closewin = true;
            }
            else if (class == GADGETUP)
            {
                switch (address->GadgetID)
                {
                    case GADFILE1:
                    {
                        flashRom(1, myCD, myWindow);
                        break;
                    }

                    case GADFILE2:
                    {
                        flashRom(2, myCD, myWindow);
                        break;
                    }

                    case GADFILEROM:
                    {
                        saveRom(myWindow);
                        displayRomInfo(&mbInfo, &mbtext);
                        WriteRomText(mbtext, Motherboard_buf, myWindow, &Motherboard);
                        free(mbtext);
                        break;
                    }

                    case GADERASE:
                    {
                        int res = rtEZRequest("This action will erase both ROM slots 1 and 2\n"
                                              "Continue anyway?",
                                              "Yes|No", NULL, NULL);

                        if (!res)
                        {
                            break;
                        }

                        WriteRomText("Erasing...", FlashROM1_buf, myWindow, &FlashROM1);
                        WriteRomText("Erasing...", FlashROM2_buf, myWindow, &FlashROM2);
                        eraseFlash(myCD);
                        getRom(1, myCD, myWindow);
                        getRom(2, myCD, myWindow);
                        break;
                    }

                    case GADABOUT:
                        {
                            char bVersion[12];
                            char bAddr[12];
                            if (NULL == myCD)
                            {
                                sprintf(bVersion, "Not found");
                                sprintf(bAddr, "N/A");
                            }
                            else
                            {
                                sprintf(bVersion, "0x%08X", (unsigned)myCD->cd_Rom.er_SerialNumber);
                                sprintf(bAddr, "0x%08X", (unsigned)myCD->cd_BoardAddr);
                            }

                            rtEZRequest("ROMulus Flash Tool %s\n"
                                        "Created by Retro Supplies\n"
                                        "https://www.retrosupplies.co.uk/\n"
                                        "This software is released under a GPLv3 license\n\n"
                                        "SST & Helper code by PR77.\n\n"
                                        "Board version: %s, addr: %s",
                                        "Cool!", NULL, NULL, VERSION, bVersion, bAddr);
                            break;
                        }

                    case GADQUIT:
                        closewin = true;
                        break;
                }
            }

            if (closewin)
                break;
        }

        if (closewin)
            break;
    };

    if (myWindow) CloseWindow(myWindow);

    *(ULONG*)myCD->cd_BoardAddr = CMD_DISABLE;
    CloseLibrary(ExpansionBase);
    return (0);
}
