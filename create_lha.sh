# Requires patool (in Debian / Ubuntu repos) and lha from https://github.com/jca02266/lha
rm ROMulus.lha
tar -cvf ROMulus.tar --transform 's,^,ROMulus/,' ROMulus ROMulus.info libs13 libs13.info libs20 libs20.info
patool repack ROMulus.tar ROMulus.lha
rm ROMulus.tar
