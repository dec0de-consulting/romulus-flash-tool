CC=m68k-amigaos-gcc
CFLAGS=-mcrt=nix13 -Os -DNDEBUG -Wall -Wno-pointer-sign -Ireqtools

#CC=vc +kick13
#CFLAGS=-Os -c99 -Ireqtools -DNDEBUG -lamiga -lauto -stack-check -double-push

all: ROMulus

ROMulus: ROMulus.c RomInfo.c Helpers.c FlashDeviceSST39.c reqtoolsstub.c reqtoolsglue.c libreqtools.a
	$(CC) $(CFLAGS) $^ -o ROMulus

clean:
	rm -f ROMulus
