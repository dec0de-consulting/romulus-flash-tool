# Requires xdftool from amitools (https://github.com/cnvogelg/amitools/)
rm -f ROMulus.adf
xdftool ROMulus.adf create
xdftool ROMulus.adf format "ROMulus"
xdftool ROMulus.adf boot install boot1x
xdftool ROMulus.adf write ROMulus
xdftool ROMulus.adf write ROMulus.info
xdftool ROMulus.adf write libs13
xdftool ROMulus.adf write libs20
xdftool ROMulus.adf write libs13.info
xdftool ROMulus.adf write libs20.info
xdftool ROMulus.adf makedir S
xdftool ROMulus.adf write startup-sequence S/Startup-Sequence
xdftool ROMulus.adf makedir C
xdftool ROMulus.adf write kick13bin/Assign C/Assign
xdftool ROMulus.adf write kick13bin/EndCLI C/EndCLI
xdftool ROMulus.adf write kick13bin/Run C/Run
xdftool ROMulus.adf write kick13bin/AddBuffers C/AddBuffers
xdftool ROMulus.adf write kick13bin/SetPatch C/Setpatch
xdftool ROMulus.adf write kick13bin/FastMemFirst C/FastMemFirst
xdftool ROMulus.adf write kick13bin/FF C/FF
xdftool ROMulus.adf write kick13bin/BindDrivers C/BindDrivers
xdftool ROMulus.adf write kick13bin/MakeDir C/MakeDir
xdftool ROMulus.adf makedir l
xdftool ROMulus.adf write kick13bin/Ram-Handler l/Ram-Handler
xdftool ROMulus.adf write kick13bin/FastFileSystem l/FastFileSystem
